#include "include/fmath.hpp"

extern "C" {
    double fmath_exp(double arg){
        return fmath::expd(arg);
    }
}
