#include "lattice.h"
#include "myrandom.h"
#include "ions.h"
#include "serialization_buffer.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

site_t init_lattice_uniform_ratio_one(int x, int y, int z, const int* sizes, void* data){
    double ratio = *((double*)data);
    return (z < sizes[2] * ratio)? 1: 0;
}

site_t init_lattice_random_ratio_one(int x, int y, int z, const int* sizes, void* data){
    double ratio = *((double*)data);
    return (mtrandf(1.0, 0) < ratio);
}

static void nearestNeighbourList(const Lattice* lattice, int site, int* nbList){
    //Find the nearest neighbours in d-dimensions
    int indices[3];
    indexToIndices(site, lattice->size, indices);
    for(int i = 0; i < 3; ++i){
        int nIndices[3];
        memcpy(nIndices, indices, 3 * sizeof(*indices));
        nIndices[i] = (indices[i] + 1 == lattice->size[i])? 0: indices[i] + 1;
        nbList[2 * i]     = indicesToIndex(nIndices, lattice->size);
        nIndices[i] = (indices[i] == 0)? lattice->size[i] - 1: indices[i] - 1;
        nbList[2 * i + 1] = indicesToIndex(nIndices, lattice->size);
    }
}

int construct_lattice(Lattice* lattice, int *size){
    memcpy(lattice->size, size, 3 * sizeof(*size));
    lattice->nSites = size[0] * size[1] * size[2];

    lattice->sites     = calloc(lattice->nSites, sizeof(*lattice->sites));
    lattice->fields[0] = calloc(lattice->nSites, sizeof(*lattice->fields[0]));
    lattice->fields[1] = calloc(lattice->nSites, sizeof(*lattice->fields[1]));
    lattice->fields[2] = calloc(lattice->nSites, sizeof(*lattice->fields[2]));
    lattice->idielectric[0] = calloc(lattice->nSites, sizeof(*lattice->idielectric[0]));
    lattice->idielectric[1] = calloc(lattice->nSites, sizeof(*lattice->idielectric[1]));
    lattice->idielectric[2] = calloc(lattice->nSites, sizeof(*lattice->idielectric[2]));

    lattice->nbLists = malloc(6 * lattice->nSites * sizeof(int));

    if(!lattice->sites || !lattice->fields[0] || !lattice->fields[1] || !lattice->fields[2] || !lattice->nbLists){
        fprintf(stderr, "Error constructing Lattice: Memory allocation error.\n");
        return 0;
    }

    for(int i = 0; i < lattice->nSites; ++i){
        nearestNeighbourList(lattice, i, lattice->nbLists + 6 * i);
    }

    return 1;
}

void destroy_lattice(Lattice* lattice){
    free(lattice->sites);
    free(lattice->fields[0]);
    free(lattice->fields[1]);
    free(lattice->fields[2]);
    free(lattice->idielectric[0]);
    free(lattice->idielectric[1]);
    free(lattice->idielectric[2]);
    free(lattice->nbLists);
}

void set_sites(Lattice* lattice, SiteSetter setter, void* data){
    int nx = lattice->size[0];
    int ny = lattice->size[1];
    int nz = lattice->size[2];
    for(int z = 0; z < nz; ++z){
        for(int y = 0; y < ny; ++y){
            for(int x = 0; x < nx; ++x){
                lattice->sites[nx * (ny * z + y) + x] = setter(x, y, z, lattice->size, data);
            }
        }
    }
}

int get_num_sites(const Lattice* lattice, site_t site_type){
    int num = 0;
    for(int i = 0; i < lattice->nSites; ++i){
        if(lattice->sites[i] == site_type) ++num;
    }
    return num;
}

void draw_ions(Lattice* lattice, const Ions* ions){
    const int* ion_pos = ions->position;

    for(int i = 0; i < ions->nIons; ++i){
        int idx = indicesToIndex(&ion_pos[3 * i], lattice->size);
        lattice->sites[idx] = (ions->charge[i] < 0)? ION_MINUS: ION_PLUS;
    }
}

void draw_plane(Lattice* lattice, int plane_id, int pos, enum eSiteTypes site_type){
    int dir0 = (plane_id + 1) % 3;
    int dir1 = (plane_id + 2) % 3;
    int size0 = lattice->size[dir0];
    int size1 = lattice->size[dir1];

    int indices[3] = {0};
    indices[plane_id] = pos;
    for(int i = 0; i < size0; ++i){
        indices[dir0] = i;
        for(int j = 0; j < size1; ++j){
            indices[dir1] = j;
            int idx = indicesToIndex(indices, lattice->size);
            lattice->sites[idx] = site_type;
        }
    }
}

//NOTE: For convenience, this function now writes each site as a site_t,
//so at least half of the memory is wasted. It might be better to use some
//compression algorithm instead of trying to write individual bits.
void serialize_lattice(const Lattice* lattice, Buffer* buffer, int write_field){
    size_t nBytes = lattice->nSites * sizeof(*lattice->sites);

    size_t total_size = 5 * sizeof(int) + nBytes;
    buffer_reserve(buffer, total_size);

    int dim = 3;
    buffer_write(buffer, &write_field, sizeof(int));
    buffer_write(buffer, &dim, sizeof(int));
    buffer_write(buffer, lattice->size, 3 * sizeof(int));
    buffer_write(buffer, lattice->sites, nBytes);

    if(write_field){
        buffer_reserve(buffer, 3 * lattice->nSites * sizeof(*lattice->fields[0]));
        buffer_write(buffer, lattice->fields[0], lattice->nSites * sizeof(*lattice->fields[0]));
        buffer_write(buffer, lattice->fields[1], lattice->nSites * sizeof(*lattice->fields[1]));
        buffer_write(buffer, lattice->fields[2], lattice->nSites * sizeof(*lattice->fields[2]));
    }
}

