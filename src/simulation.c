#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <mpi.h>
#include <inip.h>
#include "simulation.h"
#include "lattice.h"
#include "ions.h"
#include "solutes.h"
#include "myrandom.h"
#include "myrandom.h"
#include "serialization_buffer.h"
#include "utility.h"
#include "measurement.h"
#include "fmath.h"

//NOTE: If you do sequential spin updating instead of random, we get a ~3x speedup

typedef struct plates_t{
    int     total_charge[2]; //Total charge on each plate.
    double* charges[2];      //Charge on each lattice site of each plate.
    double  alpha[2];        //Adsorption strength of each plate.
}Plates;

//Calculate the total energy of the system.
static double calc_energy(const Simulation* sim){
    const Lattice* lattice    = sim->lattice;
    double* const* fields     = lattice->fields;
    double* const* idielectric = sim->lattice->idielectric;

    int nSites = lattice->nSites;

    double energy = 0.0;
    for(int i = 0; i < nSites; ++i){
        for(int d = 0; d < 3; ++d){
            energy += 0.5 * sim->params.gamma * sqr_d(fields[d][i] + sim->global_field[d]) * idielectric[d][i];
        }
        if(lattice->sites[i] == 1){
            int sums[N_SITE_TYPES] = {0};
            nb_sums(lattice, i, sums);
            energy -= sim->params.mu + 0.5 * sums[SOLVENT_B] +
                      ((sim->ions)? sim->ions->alpha[0] * sums[ION_MINUS] + sim->ions->alpha[1] * sums[ION_PLUS]: 0) +
                      ((sim->solutes)? sim->solutes->alpha[0] * sums[SOLUTE_A] + sim->solutes->alpha[1] * sums[SOLUTE_B]: 0) +
                      ((sums[PLATE_A] || sums[PLATE_B])? sim->plates->alpha[i > nSites / 2]: 0.0);
        }
    }

    return energy;
}

//Calculate the energy of a lattice region given by 'sizes', at position 'pos'.
static inline double calc_region_energy(const Simulation* sim, const int* pos, const int* sizes){
    const Lattice* lattice    = sim->lattice;
    double* const* fields     = lattice->fields;
    double* const* idielectric = sim->lattice->idielectric;

    double energy = 0.0;
    for(int x = sizes[0]; x <= sizes[1]; ++x){
        for(int y = sizes[2]; y <= sizes[3]; ++y){
            for(int z = sizes[4]; z <= sizes[5]; ++z){
                int indices[] = {x, y, z};
                for(int d = 0; d < 3; ++d) indices[d] = apply_BC(indices[d] + pos[d], lattice->size[d]);
                int idx = indicesToIndex(indices, lattice->size);
                for(int d = 0; d < 3; ++d){
                    energy += 0.5 * sim->params.gamma * sqr_d(fields[d][idx] + sim->global_field[d]) * idielectric[d][idx];
                }
                if(lattice->sites[idx] == 1){
                    int sums[N_SITE_TYPES] = {0};
                    nb_sums(lattice, idx, sums);
                    energy -= sim->params.mu + 0.5 * sums[SOLVENT_B] +
                              ((sim->ions)? sim->ions->alpha[0] * sums[ION_MINUS] + sim->ions->alpha[1] * sums[ION_PLUS]: 0) +
                              ((sim->solutes)? sim->solutes->alpha[0] * sums[SOLUTE_A] + sim->solutes->alpha[1] * sums[SOLUTE_B]: 0) +
                              ((sums[PLATE_A] || sums[PLATE_B])? sim->plates->alpha[idx > lattice->nSites / 2]: 0.0);
                }
            }
        }
    }

    return energy;
}

//Calculate the inverse dielectric of a lattice region given by 'sizes', at position 'pos'.
static inline void calc_region_idielectric(const Simulation* sim, const int* pos, const int* sizes, double* idiel){
    const Lattice* lattice    = sim->lattice;
    double* const* idielectric = sim->lattice->idielectric;

    for(int x = sizes[0]; x <= sizes[1]; ++x){
        for(int y = sizes[2]; y <= sizes[3]; ++y){
            for(int z = sizes[4]; z <= sizes[5]; ++z){
                int indices[] = {x, y, z};
                for(int d = 0; d < 3; ++d) indices[d] = apply_BC(indices[d] + pos[d], lattice->size[d]);
                int idx = indicesToIndex(indices, lattice->size);
                for(int d = 0; d < 3; ++d){
                    idiel[d] += idielectric[d][idx];
                }
            }
        }
    }
}

//Calculate the electric field and inverse dielectric of a lattice region given by 'sizes', at position 'pos'.
static inline void calc_region_field_and_idielectric(const Simulation* sim, const int* pos, const int* sizes, double* field, double* idiel){
    const Lattice* lattice    = sim->lattice;
    double* const* fields     = lattice->fields;
    double* const* idielectric = sim->lattice->idielectric;

    for(int x = sizes[0]; x <= sizes[1]; ++x){
        for(int y = sizes[2]; y <= sizes[3]; ++y){
            for(int z = sizes[4]; z <= sizes[5]; ++z){
                int indices[] = {x, y, z};
                for(int d = 0; d < 3; ++d) indices[d] = apply_BC(indices[d] + pos[d], lattice->size[d]);
                int idx = indicesToIndex(indices, lattice->size);
                for(int d = 0; d < 3; ++d){
                    field[d] += fields[d][idx] * idielectric[d][idx];
                    idiel[d] += idielectric[d][idx];
                }
            }
        }
    }
}

//Check if the value of the inverse dielectric we have stored for each lattice link
//is consistent, by calculating it from the neighboring sites.
static inline bool check_dielectric(const Simulation* sim){
    const Lattice* lattice = sim->lattice;
    const double* dielectric = sim->params.dielectric;
    double* const* idielectric = sim->lattice->idielectric;

    for(int i = 0; i < lattice->nSites; ++i){
        const int* nbList = &sim->lattice->nbLists[6 * i];
        for(int d = 0; d < 3; ++d){
            double true_idiel = 0.5 * (1.0 / dielectric[lattice->sites[i]] + 1.0 / dielectric[lattice->sites[nbList[2 * d]]]);
            if(fabs(idielectric[d][i] - true_idiel) > 1.0e-9) return true;
        }
    }
    return false;
}

//Check if Gauss's law holds everywhere on the lattice.
static bool check_gauss(const Simulation* sim){
    bool is_gauss_violated = false;
    for(int z = 0; z < sim->lattice->size[2]; ++z){
        for(int y = 0; y < sim->lattice->size[1]; ++y){
            for(int x = 0; x < sim->lattice->size[0]; ++x){
                double charge;
                int indices[] = {x, y, z};
                int idx = indicesToIndex(indices, sim->lattice->size);
                double sum_field = 0.0;
                if(sim->plates && z == 0){
                    charge    = sim->plates->charges[0][idx];
                    sum_field = sim->lattice->fields[2][idx];
                }
                else if(sim->plates && z == sim->lattice->size[2] - 1){
                    charge    = sim->plates->charges[1][x + y * sim->lattice->size[0]];
                    int indices_neg[] = {x, y, z - 1};
                    int idxd = indicesToIndex(indices_neg, sim->lattice->size);
                    sum_field = -sim->lattice->fields[2][idxd];
                }
                else{
                    charge = CHARGE(sim->ions, sim->lattice->sites[idx]);
                    for(int d = 0; d < 3; ++d){
                        int indices_neg[3];
                        memcpy(indices_neg, indices, 3 * sizeof(int));
                        indices_neg[d] = apply_BC(indices_neg[d] - 1, sim->lattice->size[d]);
                        int idxd = indicesToIndex(indices_neg, sim->lattice->size);
                        sum_field += sim->lattice->fields[d][idx] - sim->lattice->fields[d][idxd];
                    }
                }
                if(fabs(sum_field - charge) > 1e-8){
                    printf("Gauss's law violated at %d, %d, %d\n", x, y, z);
                    printf("Flux: %f, Charge: %f\n", sum_field, charge);
                    is_gauss_violated = true;
                }
            }
        }
    }
    return is_gauss_violated;
}

//Calculate and store the value of the inverse dielectric at site 'site'
static inline void update_dielectric(Simulation* sim, int site){
    double** lattice_idielectric = sim->lattice->idielectric;
    const double* dielectric = sim->params.dielectric;
    const int* nbList = &sim->lattice->nbLists[6 * site];

    site_t site_type = sim->lattice->sites[site];
    for(int d = 0; d < 3; ++d){
        int idxd = nbList[2 * d + 1];
        lattice_idielectric[d][site] = 0.5 * (1.0 / dielectric[site_type] + 1.0 / dielectric[sim->lattice->sites[nbList[2 * d]]]);
        lattice_idielectric[d][idxd] = 0.5 * (1.0 / dielectric[site_type] + 1.0 / dielectric[sim->lattice->sites[idxd]]);
    }
}

//Translate a solute particle by swapping it with a solvent particle,
//or with a solute of the opposite species.
static inline int solute_step(Simulation* sim, int rN){
    const Lattice* lattice = sim->lattice;

    int move = mtrandi(6);

    int *pos = &sim->solutes->position[3 * rN];
    int site_idx = indicesToIndex(pos, lattice->size);
    int new_site_idx = lattice->nbLists[6 * site_idx + move];

    if(lattice->sites[site_idx] == lattice->sites[new_site_idx]) return 0;
    if(lattice->sites[new_site_idx] > SOLVENT_B && lattice->sites[new_site_idx] < SOLUTE_A) return 0;

    int dir = move / 2;
    int dx  = 1 - 2 * (move % 2);

    int new_pos[3];
    memcpy(new_pos, pos, 3 * sizeof(int));
    new_pos[dir] = (new_pos[dir] + dx + lattice->size[dir]) % lattice->size[dir];

    int region_size[] = {
        -1, 1,
        -1, 1,
        -1, 1
    };

    region_size[2 * dir + 1 - move % 2] += dx;

    double dE = -calc_region_energy(sim, pos, region_size);

    double idielectric[3] = {0.0};
    calc_region_idielectric(sim, pos, region_size, idielectric);

    swap_sites(sim->lattice->sites, site_idx, new_site_idx);
    update_dielectric(sim, site_idx);
    update_dielectric(sim, new_site_idx);

    dE += calc_region_energy(sim, pos, region_size);

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        calc_region_idielectric(sim, pos, region_size, idielectric);
        for(int i = 0; i < 3; ++i) sim->total_idielectric[i] += idielectric[i];
        pos[dir] = new_pos[dir];
        sim->dE += dE;
        return 1;
    }

    swap_sites(sim->lattice->sites, site_idx, new_site_idx);
    update_dielectric(sim, site_idx);
    update_dielectric(sim, new_site_idx);

    return 0;
}

//Grand Canonical solvent step.
static inline int solvent_step(Simulation* sim, int rN) {
    const Lattice* lattice = sim->lattice;
    site_t* sites = lattice->sites;

    site_t site_type = sites[rN];

    if(site_type > SOLVENT_B){
        if(sim->tmmc) ++sim->tmmc->collection_matrix[3 * (sim->num_bsites - sim->tmmc->n_low) + 1];
        return 0;
    }

    site_t ds = !site_type - site_type;

    if(sim->tmmc &&
      ((sim->num_bsites + ds < sim->tmmc->n_low) ||
       (sim->num_bsites + ds > sim->tmmc->n_high))
    ){
        ++sim->tmmc->collection_matrix[3 * (sim->num_bsites - sim->tmmc->n_low) + 1];
        return 0;
    }

    int sums[N_SITE_TYPES] = {0};
    nb_sums(lattice, rN, sums);
    double dE = -ds * (sim->params.mu + sums[SOLVENT_B] +
                      ((sim->ions)? sim->ions->alpha[0] * sums[ION_MINUS] + sim->ions->alpha[1] * sums[ION_PLUS]: 0) +
                      ((sim->solutes)? sim->solutes->alpha[0] * sums[SOLUTE_A] + sim->solutes->alpha[1] * sums[SOLUTE_B]: 0) +
                      ((sums[PLATE_A] || sums[PLATE_B])? sim->plates->alpha[rN > lattice->nSites / 2]: 0.0));

    const int* nbList = &lattice->nbLists[6 * rN];
    const double* dielectric = sim->params.dielectric;
    double** lattice_idielectric = sim->lattice->idielectric;
    double idielectric_new[6];
    //For each dimension, calculate the value of the new inverse dielectric
    //due to the solvent type swap and calculate the energy difference
    //due to the swap.
    for(int d = 0; d < 3; ++d){
        int idxd = nbList[2 * d + 1];
        double eps1  = lattice_idielectric[d][rN]; //inverse dielectric before
        double eps2  = lattice_idielectric[d][rN] + 0.5 * (1.0 / dielectric[!site_type] - 1.0 / dielectric[site_type]); //inverse dielectric after
        double eps1n = lattice_idielectric[d][idxd]; //inverse dielectric before in the negative direction
        double eps2n = lattice_idielectric[d][idxd] + 0.5 * (1.0 / dielectric[!site_type] - 1.0 / dielectric[site_type]); //inverse dielectric after in the negative direction

        dE += 0.5 * sim->params.gamma * sqr_d(sim->lattice->fields[d][rN] + sim->global_field[d]) * (eps2 - eps1) +
              0.5 * sim->params.gamma * sqr_d(sim->lattice->fields[d][idxd] + sim->global_field[d]) * (eps2n - eps1n);

        idielectric_new[2 * d]     = eps2;
        idielectric_new[2 * d + 1] = eps2n;
    }

    bool accepted;
    if(!sim->tmmc) accepted = (dE < 0.0) || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE);
    else{
        double acceptance = exp(-sim->params.beta * dE);
        int cmidx = sim->num_bsites - sim->tmmc->n_low;
        double *c_m = sim->tmmc->collection_matrix + 3 * cmidx;

        c_m[1]                += (acceptance > 1.0)? 0.0: 1.0 - acceptance;
        c_m[2 * (!site_type)] += (acceptance > 1.0)? 1.0: acceptance;

        const double* wF = sim->tmmc->state_weights;
        double wFnew = wF[cmidx + ds];
        double wFold = wF[cmidx];
        acceptance *= exp(wFnew - wFold);
        accepted = (acceptance > 1.0) || (mtrandf(1.0, 0) < acceptance);
    }

    if(accepted){
        sites[rN] = !site_type;
        for(int d = 0; d < 3; ++d){
            int idxd = nbList[2 * d + 1];
            double idielectric_diff1 = idielectric_new[2 * d] - lattice_idielectric[d][rN];
            double idielectric_diff2 = idielectric_new[2 * d + 1] - lattice_idielectric[d][idxd];
            sim->total_idielectric[d] += idielectric_diff1 + idielectric_diff2;
            sim->total_field[d] += sim->lattice->fields[d][rN] * idielectric_diff1;
            sim->total_field[d] += sim->lattice->fields[d][idxd] * idielectric_diff2;
            lattice_idielectric[d][rN]   = idielectric_new[2 * d];
            lattice_idielectric[d][idxd] = idielectric_new[2 * d + 1];
        }
        sim->num_bsites += ds;
        sim->dE += dE;

        return 1;
    }

    return 0;
}

//Translate a solvent particle by exchanging it with a neighboring
//one of the opposite species.
static inline int solvent_canonical_step(Simulation* sim, int site_idx){
    const Lattice* lattice = sim->lattice;

    int move = mtrandi(6);
    int new_site_idx = lattice->nbLists[6 * site_idx + move];

    //Only a SOLVENT_B and a SOLVENT_A can be exchanged.
    if((lattice->sites[site_idx] + lattice->sites[new_site_idx]) != 1) return 0;

    const double* dielectric     = sim->params.dielectric;
    double** lattice_idielectric = sim->lattice->idielectric;

    //When swapping a SOLVENT_A with a SOLVENT_B, the value of the inverse dielectric
    //will change by the amount as calculated below.
    double idielectric_diff = 0.5 * (2 * lattice->sites[site_idx] - 1) * (1.0 / dielectric[SOLVENT_B] - 1.0 / dielectric[SOLVENT_A]);

    double dE = 0.0;
    int site_duo[] = {site_idx, new_site_idx};
    int sums[N_SITE_TYPES] = {0};

    //The energy difference for the move, is due to a change in the
    //value of the inverse dielectric.
    for(int i = 0; i < 2; ++i){
        int curr_idx = site_duo[i];
        const int* nbList = &lattice->nbLists[6 * curr_idx];
        double signed_idielectric_diff = (2 * i - 1) * idielectric_diff;

        for(int d = 0; d < 3; ++d){
            int idx = nbList[2 * d + 1];
            dE += 0.5 * sim->params.gamma * signed_idielectric_diff * (
                sqr_d(lattice->fields[d][curr_idx] + sim->global_field[d]) +
                sqr_d(lattice->fields[d][idx] + sim->global_field[d])
            );
        }

        int ds = 1 - 2 * lattice->sites[curr_idx];
        int temp_sums[N_SITE_TYPES] = {0};
        nb_sums(lattice, curr_idx, temp_sums);
        for(int s = 0; s < N_SITE_TYPES; ++s) sums[s] += temp_sums[s] * ds;
        sums[lattice->sites[curr_idx]] += ds;
    }

    dE -= sums[SOLVENT_B] +
          ((sim->ions)? sim->ions->alpha[0] * sums[ION_MINUS] + sim->ions->alpha[1] * sums[ION_PLUS]: 0) +
          ((sim->solutes)? sim->solutes->alpha[0] * sums[SOLUTE_A] + sim->solutes->alpha[1] * sums[SOLUTE_B]: 0) +
          ((sim->plates)? (sums[PLATE_A] + sums[PLATE_B]) * sim->plates->alpha[site_idx > lattice->nSites / 2]: 0);

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        for(int i = 0; i < 2; ++i){
            int curr_idx = site_duo[i];
            const int* nbList = &lattice->nbLists[6 * curr_idx];
            double signed_idielectric_diff = (2 * i - 1) * idielectric_diff;
            for(int d = 0; d < 3; ++d){
                int idx = nbList[2 * d + 1];
                lattice_idielectric[d][curr_idx] += signed_idielectric_diff;
                lattice_idielectric[d][idx] += signed_idielectric_diff;
                sim->total_field[d] += signed_idielectric_diff * (lattice->fields[d][curr_idx] + lattice->fields[d][idx]);
                sim->total_idielectric[d] += 2.0 * signed_idielectric_diff;
            }
        }
        swap_sites(lattice->sites, site_idx, new_site_idx);
        sim->dE += dE;
        return 1;
    }

    return 0;
}

//Attempt to change the field circulation of a plaquette with id 'plaquette_id'.
static inline int field_step(Simulation* sim, int plaquette_id){
    double** field = sim->lattice->fields;
    double** lattice_idielectric = sim->lattice->idielectric;

    //We identify plaquettes by a combination of site id and the id of the direction
    //out of 3 possible directions, one for each dimension. Thus, the plaquette's id
    //is: plaquette_id = 3 * site_id + dir_id

    //Calculate site id and direction corresponding to plaquette
    int site, dir;
    {
        int size_xy = sim->lattice->size[0] * sim->lattice->size[1];
        int temp_id = plaquette_id / size_xy;
        site = (plaquette_id % size_xy) + (temp_id / 3) * size_xy;
        dir  = temp_id % 3;
    }

    //dir1, dir2 here denote the two indices corresponding to the
    //directions of the plate defined by the plaquette.
    int dir1, dir2, idx1, idx2;
    {
        int indices1[3], indices2[3];
        indexToIndices(site, sim->lattice->size, indices1);
        memcpy(indices2, indices1, 3 * sizeof(int));

        dir1 = dir; //(dir + 2) % 3
        dir2 = (dir == 2)? 0: dir + 1; //(dir + 1) % 3
        indices1[dir1] = ((indices1[dir1] + 1) < sim->lattice->size[dir1])? (indices1[dir1] + 1): 0;
        indices2[dir2] = ((indices2[dir2] + 1) < sim->lattice->size[dir2])? (indices2[dir2] + 1): 0;

        idx1 = indicesToIndex(indices2, sim->lattice->size);
        idx2 = indicesToIndex(indices1, sim->lattice->size);
    }

    double noise = mtrandf(sim->params.max_field, 1);
    double dE = (noise * lattice_idielectric[dir1][site]) * (field[dir1][site] + sim->global_field[dir1] + 0.5 * noise) -
                (noise * lattice_idielectric[dir1][idx1]) * (field[dir1][idx1] + sim->global_field[dir1] - 0.5 * noise) -
                (noise * lattice_idielectric[dir2][site]) * (field[dir2][site] + sim->global_field[dir2] - 0.5 * noise) +
                (noise * lattice_idielectric[dir2][idx2]) * (field[dir2][idx2] + sim->global_field[dir2] + 0.5 * noise);

    dE *= sim->params.gamma;

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        sim->total_field[dir1] += noise * lattice_idielectric[dir1][site];
        sim->total_field[dir1] -= noise * lattice_idielectric[dir1][idx1];
        sim->total_field[dir2] -= noise * lattice_idielectric[dir2][site];
        sim->total_field[dir2] += noise * lattice_idielectric[dir2][idx2];
        field[dir1][site] += noise;
        field[dir1][idx1] -= noise;
        field[dir2][site] -= noise;
        field[dir2][idx2] += noise;
        sim->dE += dE;
        return 1;
    }
    return 0;
}

//Make a random choice based on the probability weights 'weights'.
static inline int random_choice(int n, const double* weights){
    double norm = 0.0;
    double totals[n];
    for(int i = 0; i < n; ++i){
        norm += weights[i];
        totals[i] = norm;
    }

    double choice = mtrandf(norm, 0);
    int dir = 0;
    for(int i = 0; i < n; ++i){
        if(choice < totals[i]){
            dir = i;
            break;
        }
    }

    return dir;
}

//Worm field update step. A closed path over which the electric field is
//updated is generated by creating a positive and negative ion at site
//'site' and subsequently translating one of the ions until it meets
//the other one.
static inline int worm_step(Simulation* sim, int site){
    double* const* lattice_idielectric = sim->lattice->idielectric;
    const double dq = 1.0 / sqrt(sim->params.gamma);

    int indices[3];
    indexToIndices(site, sim->lattice->size, indices);

    //Backup the field in case we have to reject, although the chance is
    //very small.
    double backup_total_field[3];
    memcpy(backup_total_field, sim->total_field, 3 * sizeof(double));
    double* backup_fields[3];
    for(int d = 0; d < 3; ++d){
        backup_fields[d] = malloc(sim->lattice->nSites * sizeof(double));
        memcpy(backup_fields[d], sim->lattice->fields[d], sim->lattice->nSites * sizeof(double));
    }

    double noise = mtrandf(dq, 1);
    double dE = 0.0;

    double init_norm = 0.0;
    int new_site;
    {
        //Calculate weights
        double args[6];
        for(int d = 0; d < 3; ++d){
            int indices_neg[3];
            memcpy(indices_neg, indices, 3 * sizeof(int));
            indices_neg[d] = apply_BC(indices_neg[d] - 1, sim->lattice->size[d]);
            int idxd = indicesToIndex(indices_neg, sim->lattice->size);
            args[2 * d]     = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise + sim->lattice->fields[d][site] + sim->global_field[d]) * lattice_idielectric[d][site];
            args[2 * d + 1] = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise - sim->lattice->fields[d][idxd] - sim->global_field[d]) * lattice_idielectric[d][idxd];
        }

        //fmath_exp_v(weights, 6);
        double weights[6];
        for(int i = 0; i < 6; ++i){
            weights[i] = min(1.0, fmath_exp(args[i]));
            init_norm += weights[i];
        }
        if(sim->plates){
            if(indices[2] == 1){
                init_norm -= weights[5];
                weights[5] = 0.0;
            }
            else if(indices[2] + 2 == sim->lattice->size[2]){
                init_norm -= weights[4];
                weights[4] = 0.0;
            }
        }
        int dir = random_choice(6, weights);
        dE -= args[dir] / sim->params.beta;

        indices[dir / 2] = apply_BC(indices[dir / 2] + 1 - 2 * (dir % 2), sim->lattice->size[dir / 2]);

        new_site = indicesToIndex(indices, sim->lattice->size);
        if(dir % 2 == 0){
            sim->lattice->fields[dir / 2][site] += noise;
            sim->total_field[dir / 2] += noise * sim->lattice->idielectric[dir / 2][site];
        }
        else{
            sim->lattice->fields[dir / 2][new_site] -= noise;
            sim->total_field[dir / 2] -= noise * sim->lattice->idielectric[dir / 2][new_site];
        }
    }

    while(new_site != site){
        //Calculate weights
        double args[6];
        for(int d = 0; d < 3; ++d){
            int indices_neg[3];
            memcpy(indices_neg, indices, 3 * sizeof(int));
            indices_neg[d] = apply_BC(indices_neg[d] - 1, sim->lattice->size[d]);
            int idxd = indicesToIndex(indices_neg, sim->lattice->size);
            args[2 * d]     = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise + sim->lattice->fields[d][new_site] + sim->global_field[d]) * lattice_idielectric[d][new_site];
            args[2 * d + 1] = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise - sim->lattice->fields[d][idxd] - sim->global_field[d]) * lattice_idielectric[d][idxd];
        }

        //fmath_exp_v(weights, 6);
        double norm = 0.0;
        double weights[6];
        for(int i = 0; i < 6; ++i){
            weights[i] = min(1.0, fmath_exp(args[i]));
            norm += weights[i];
        }
        if(sim->plates){
            if(indices[2] == 1){
                norm -= weights[5];
                weights[5] = 0.0;
            }
            else if(indices[2] + 2 == sim->lattice->size[2]){
                norm -= weights[4];
                weights[4] = 0.0;
            }
        }
        int dir = random_choice(6, weights);
        dE -= args[dir] / sim->params.beta;

        indices[dir / 2] = apply_BC(indices[dir / 2] + 1 - 2 * (dir % 2), sim->lattice->size[dir / 2]);
        int old_site = new_site;
        new_site = indicesToIndex(indices, sim->lattice->size);

        if(dir % 2 == 0){
            sim->lattice->fields[dir / 2][old_site] += noise;
            sim->total_field[dir / 2] += noise * sim->lattice->idielectric[dir / 2][old_site];
        }
        else{
            sim->lattice->fields[dir / 2][new_site] -= noise;
            sim->total_field[dir / 2] -= noise * sim->lattice->idielectric[dir / 2][new_site];
        }
    }

    double end_norm = 0.0;
    {
        //Calculate weights
        double args[6];
        for(int d = 0; d < 3; ++d){
            int indices_neg[3];
            memcpy(indices_neg, indices, 3 * sizeof(int));
            indices_neg[d] = apply_BC(indices_neg[d] - 1, sim->lattice->size[d]);
            int idxd = indicesToIndex(indices_neg, sim->lattice->size);
            args[2 * d]     = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise + sim->lattice->fields[d][site] + sim->global_field[d]) * lattice_idielectric[d][site];
            args[2 * d + 1] = -sim->params.beta * sim->params.gamma * noise * (0.5 * noise - sim->lattice->fields[d][idxd] - sim->global_field[d]) * lattice_idielectric[d][idxd];
        }

        //fmath_exp_v(weights, 6);
        double weights[6];
        for(int i = 0; i < 6; ++i){
            weights[i] = min(1.0, fmath_exp(args[i]));
            end_norm += weights[i];
        }
        if(sim->plates){
            if(indices[2] == 1){
                end_norm -= weights[5];
                weights[5] = 0.0;
            }
            else if(indices[2] + 2 == sim->lattice->size[2]){
                end_norm -= weights[4];
                weights[4] = 0.0;
            }
        }
    }

    double rejection = 1.0 - min(1.0, init_norm / end_norm);
    if(rejection > 0.0 && mtrandf(1.0, 0) < rejection){
        for(int d = 0; d < 3; ++d){
            memcpy(sim->lattice->fields[d], backup_fields[d], sim->lattice->nSites * sizeof(double));
            free(backup_fields[d]);
        }
        memcpy(sim->total_field, backup_total_field, 3 * sizeof(double));
        return 0;
    }

    sim->dE += dE;

    for(int d = 0; d < 3; ++d) free(backup_fields[d]);

    return 1;
}

//Attempt to translate a random amount of charge at site 'site'
//to a neighboring site.
static inline int plate_step(Simulation* sim, int site){
    int size_xy  = sim->lattice->size[0] * sim->lattice->size[1];
    int nx       = sim->lattice->size[0];
    int plate_id = site % 2;
    int sid      = site / 2;

    int move  = mtrandi(4);
    int dir   = move / 2;
    int dx    = 1 - 2 * (move % 2);

    int x = sid % nx;
    int y = sid / nx;

    int new_pos[] = {x, y, (sim->lattice->size[2] - 1) * plate_id};

    int site0, site1;
    {
        int old_site = indicesToIndex(new_pos, sim->lattice->size);
        new_pos[dir] = apply_BC(new_pos[dir] + dx, sim->lattice->size[dir]);
        int new_site = indicesToIndex(new_pos, sim->lattice->size);

        site0 = (dx > 0)? old_site: new_site;
        site1 = (dx > 0)? new_site: old_site;
    }

    int fsites[3];
    switch(plate_id){
        case 0:
            fsites[0] = site0;
            fsites[1] = site0 + size_xy;
            fsites[2] = site1;
            break;
        case 1:
            fsites[0] = site1 - size_xy;
            fsites[1] = site0 - size_xy;
            fsites[2] = site0 - size_xy;
            break;
        default:
           printf("Error, unknown plate id encountered!!!\n");
           return 0;
    }

    double* fields[3] = {
        &sim->lattice->fields[2][fsites[0]],
        &sim->lattice->fields[dir][fsites[1]],
        &sim->lattice->fields[2][fsites[2]],
    };

    double noise  = mtrandf(sim->params.max_plate, 1);
    double dnoise = dx * noise;
    //double dE = dnoise * (1.5 * dnoise + -*fields[0] - *fields[1] + *fields[2] - sim->global_field[dir]);
    double** lattice_idielectric = sim->lattice->idielectric;
    double dE = (dnoise * lattice_idielectric[2][fsites[2]]) * (*fields[2] + sim->global_field[2] + 0.5 * dnoise) -
                (dnoise * lattice_idielectric[2][fsites[0]]) * (*fields[0] + sim->global_field[2] - 0.5 * dnoise) -
                (dnoise * lattice_idielectric[dir][fsites[1]]) * (*fields[1] + sim->global_field[dir] - 0.5 * dnoise);

    dE *= sim->params.gamma;

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        sim->total_field[dir] -= dnoise * lattice_idielectric[dir][fsites[1]];
        sim->total_field[2]   += dnoise * lattice_idielectric[2][fsites[2]];
        sim->total_field[2]   -= dnoise * lattice_idielectric[2][fsites[0]];
        *fields[0] -= dnoise;
        *fields[1] -= dnoise;
        *fields[2] += dnoise;
        sim->plates->charges[plate_id][sid] -= noise;
        sim->plates->charges[plate_id][new_pos[0] + new_pos[1] * nx] += noise;
        sim->dE += dE;
        return 1;
    }
    return 0;
}

//Attempt to modify the global electric field.
static inline int global_field_step(Simulation* sim){
    double noise[3] = {
        mtrandf(sim->params.max_Gfield, 1),
        mtrandf(sim->params.max_Gfield, 1),
        (sim->plates)? 0.0: mtrandf(sim->params.max_Gfield, 1)
    };

    double dE = sim->total_field[0] * noise[0] +
                sim->total_field[1] * noise[1] +
                sim->total_field[2] * noise[2];

    for(int i = 0; i < 3; ++i) dE += noise[i] * sim->total_idielectric[i] * (sim->global_field[i] + 0.5 * noise[i]);
    dE *= sim->params.gamma;

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        for(int i = 0; i < 3; ++i) sim->global_field[i] += noise[i];
        sim->dE += dE;
        return 1;
    }
    return 0;
}

//Translate an ion with id 'ion_id', by swapping it with a solvent
//particle. The field is update using the move proposed by Duncan,
//Sedgewick, and Coalson (DSC).
//TODO: allow swapping with a solute particles, and ion particles of opposite charge.
static inline int ion_step(Simulation* sim, int ion_id){
    const int* size = sim->lattice->size;
    int *pos = &sim->ions->position[3 * ion_id];

    int move  = mtrandi(6);
    int dir   = move / 2;
    int dx    = 1 - 2 * (move % 2);

    int new_pos[3];
    memcpy(new_pos, pos, 3 * sizeof(int));
    new_pos[dir] = (new_pos[dir] + dx + size[dir]) % size[dir];
    int new_site = indicesToIndex(new_pos, size);

    if(sim->lattice->sites[new_site] > SOLVENT_B) return 0;

    int old_site = indicesToIndex(pos, size);

    //TODO: We can still use DSC here, but we should fallback to the 2D one.
    if(sim->plates && (pos[2] == 1 || pos[2] + 2 == size[2]) && dir != 2){
        double* field = &sim->lattice->fields[dir][(dx > 0)? old_site: new_site];
        double field_old = *field;
        double field_new = *field - dx * sim->ions->charge[ion_id];
        //double dE = 0.5 * (sqr_d(field_new + sim->global_field[dir]) - sqr_d(*field + sim->global_field[dir]));
        int region_size[] = {
            -1, 1,
            -1, 1,
            -1, 1
        };
        region_size[2 * dir + 1 - move % 2] += dx;

        double dE = -calc_region_energy(sim, pos, region_size);
        double idielectric[3] = {0.0};
        double total_field[3] = {0.0};
        calc_region_field_and_idielectric(sim, pos, region_size, total_field, idielectric);
        for(int i = 0; i < 3; ++i){
            total_field[i] *= -1;
            idielectric[i] *= -1;
        }
        {
            swap_sites(sim->lattice->sites, old_site, new_site);
            *field = field_new;
            update_dielectric(sim, old_site);
            update_dielectric(sim, new_site);
        }
        calc_region_field_and_idielectric(sim, pos, region_size, total_field, idielectric);
        dE += calc_region_energy(sim, pos, region_size);

        if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
            for(int i = 0; i < 3; ++i){
                sim->total_field[i] += total_field[i];
                sim->total_idielectric[i] += idielectric[i];
            }
            pos[dir] = new_pos[dir];
            sim->dE += dE;
            return 1;
        }
        else{
            *field = field_old;
            swap_sites(sim->lattice->sites, old_site, new_site);
            update_dielectric(sim, old_site);
            update_dielectric(sim, new_site);
        }
        return 0;
    }

    int dir0 = dir;            //When dir = 0, x y z
    int dir1 = (dir &  1) ^ 1; //     dir = 1, y x z
    int dir2 = (dir & ~1) ^ 2; //     dir = 2, z y x

    //Calculate the field at the 13 links as required by the DSC move.
    double** fields = sim->lattice->fields;
    double* field[13];
    {

        int indices0[3], indices1[3], indices2[3], indices3[3], indices4[3], indices5[3];
        memcpy(indices0, (dx < 0)? pos: new_pos, 3 * sizeof(int));
        memcpy(indices1, (dx > 0)? pos: new_pos, 3 * sizeof(int));
        memcpy(indices2, (dx > 0)? pos: new_pos, 3 * sizeof(int));
        memcpy(indices3, (dx < 0)? pos: new_pos, 3 * sizeof(int));
        memcpy(indices4, (dx > 0)? pos: new_pos, 3 * sizeof(int));
        memcpy(indices5, (dx > 0)? pos: new_pos, 3 * sizeof(int));

        indices0[dir1] = apply_BC(indices0[dir1] - 1, size[dir1]);
        indices1[dir1] = apply_BC(indices1[dir1] + 1, size[dir1]);
        indices2[dir1] = apply_BC(indices2[dir1] - 1, size[dir1]);
        indices3[dir2] = apply_BC(indices3[dir2] - 1, size[dir2]);
        indices4[dir2] = apply_BC(indices4[dir2] + 1, size[dir2]);
        indices5[dir2] = apply_BC(indices5[dir2] - 1, size[dir2]);

        int idx1 = indicesToIndex(indices2, size);
        int idx2 = indicesToIndex(indices5, size);
        int site0 = (dx > 0)? old_site: new_site;
        int site1 = (dx < 0)? old_site: new_site;

        field[0]  = &fields[dir0][site0];

        field[1]  = &fields[dir0][indicesToIndex(indices1, size)];
        field[2]  = &fields[dir0][idx1];
        field[3]  = &fields[dir0][indicesToIndex(indices4, size)];
        field[4]  = &fields[dir0][idx2];

        field[5]  = &fields[dir1][site1];
        field[6]  = &fields[dir1][idx1];
        field[7]  = &fields[dir1][site0];
        field[8]  = &fields[dir1][indicesToIndex(indices0, size)];

        field[9]  = &fields[dir2][site1];
        field[10] = &fields[dir2][idx2];
        field[11] = &fields[dir2][site0];
        field[12] = &fields[dir2][indicesToIndex(indices3, size)];
    }

    int charge = sim->ions->charge[ion_id];
    double field_new[13];
    double field_old[13];
    for(int i = 0; i < 13; ++i){
        field_old[i] = *field[i];
        field_new[i] = *field[i];
    }

#define a (1.0 / 7.0) //The field displacement
    field_new[0]  -= 3.0 * (a * dx) * charge;
    for(int i = 0; i < 4; ++i){
        field_new[i + 1]  -= (a * dx) * charge;
        field_new[4 * (i % 2) + (i / 2) + 5]  += (a * dx) * charge;
        field_new[4 * (i % 2) + (i / 2) + 7]  -= (a * dx) * charge;
    }
#undef a

    int region_size[] = {
        -1, 1,
        -1, 1,
        -1, 1
    };
    region_size[2 * dir + 1 - move % 2] += dx;

    double dE = -calc_region_energy(sim, pos, region_size);
    double idielectric[3] = {0.0};
    double total_field[3] = {0.0};
    calc_region_field_and_idielectric(sim, pos, region_size, total_field, idielectric);
    for(int i = 0; i < 3; ++i){
        total_field[i] *= -1;
        idielectric[i] *= -1;
    }

    //Do the move
    {
        for(int i = 0; i < 13; ++i) *field[i] = field_new[i];
        swap_sites(sim->lattice->sites, old_site, new_site);
        update_dielectric(sim, old_site);
        update_dielectric(sim, new_site);
    }

    calc_region_field_and_idielectric(sim, pos, region_size, total_field, idielectric);
    dE += calc_region_energy(sim, pos, region_size);

    if(dE < 0.0 || metropolis(mtrandf(1.0, 0), -sim->params.beta * dE)){
        for(int i = 0; i < 3; ++i){
            sim->total_field[i] += total_field[i];
            sim->total_idielectric[i] += idielectric[i];
        }
        pos[dir] = new_pos[dir];
        sim->dE += dE;
        return 1;
    }
    else{
        //Undo the move
        for(int i = 0; i < 13; ++i) *field[i] = field_old[i];
        swap_sites(sim->lattice->sites, old_site, new_site);
        update_dielectric(sim, old_site);
        update_dielectric(sim, new_site);
    }
    return 0;
}

static inline double calc_trans_prob(const Simulation *sim, int a, int b){
    double cAtoB = sim->tmmc->collection_matrix[3 * a + 1 + (b - a)];

    if(cAtoB == 0.0) return 0.0;

    double sum = 0.0;
    for(int i = 0; i < 3; ++i) sum += sim->tmmc->collection_matrix[3 * a + i];

    return cAtoB / sum;
}

static void update_weights(Simulation* sim){
    double* wF = sim->tmmc->state_weights;
    wF[0] = 0.0;
    int nTMMC = sim->tmmc->n_tot;
    for(int i = 0; i < nTMMC - 1; ++i){
        double tProbAB = calc_trans_prob(sim, i, i + 1);
        double tProbBA = calc_trans_prob(sim, i + 1, i);
        if(tProbAB != 0.0 && tProbBA != 0.0){
            wF[i + 1] = wF[i] - log(tProbAB / tProbBA);//use logA - logB for better precision
        }
        else wF[i + 1] = wF[i];
    }
}

//Initialize the electric field of the system to obey Gauss's law by a taking
//Hamiltonian path through the lattice. Such a path visits each site just once
//and traverses each link either once or zero times.
static void init_fields(Simulation* sim){
    const site_t* sites = sim->lattice->sites;

    sim->global_field[0] = 0.0; sim->global_field[1] = 0.0; sim->global_field[2] = 0.0;

    int nx = sim->lattice->size[0];
    int ny = sim->lattice->size[1];
    int nz = sim->lattice->size[2];

    int dir_x = 1;
    int dir_y = 1;
    double total_charge = 0.0;

    int z_start = 0;
    int z_end   = nz;
    if(sim->plates){
        z_start = 1;
        z_end   = nz - 1;
        for(int i = 0; i < nx * ny; ++i){
            sim->lattice->fields[2][i] = sim->plates->charges[0][i];
            sim->lattice->fields[2][nx * ny * (nz - 2) + i] = -sim->plates->charges[1][i];
        }
    }

    int x = 0;
    int y = 0;
    for(int z = z_start; z < z_end; ++z){
        for(int j = 0; j < ny; ++j){
            for(int i = 0; i < nx; ++i){
                int idx = x + (y + z * ny) * nx;
                total_charge += CHARGE(sim->ions, sites[idx]);
                if(sim->plates){
                    total_charge += (z == z_start)? sim->plates->charges[0][x + nx * y]:
                                    (z == z_end - 1)? sim->plates->charges[1][x + nx * y]: 0;
                }
                if(i == nx - 1){
                    if(j == ny - 1){
                        if(z < z_end - 1){
                            sim->lattice->fields[2][idx] = total_charge;
                        }
                    }
                    else{
                        sim->lattice->fields[1][idx - nx * (dir_y < 0)] = dir_y * total_charge;
                    }
                }
                else{
                    sim->lattice->fields[0][idx - (dir_x < 0)] = dir_x * total_charge;
                }
                x += dir_x;
            }
            x -= dir_x;
            dir_x = -dir_x;
            y += dir_y;
        }
        y -= dir_y;
        dir_y = -dir_y;
    }

    check_gauss(sim);
}

//Cache the value of the inverse dielectric at each link.
static void init_dielectric(Simulation* sim){
    const site_t* sites = sim->lattice->sites;
    double* const* lattice_idielectric = sim->lattice->idielectric;

    double* dielectric = sim->params.dielectric;
    for(int i = 0; i < sim->lattice->nSites; ++i){
        const int* nbList = &sim->lattice->nbLists[6 * i];
        for(int d = 0; d < 3; ++d){
            lattice_idielectric[d][i] = 0.5 * (1.0 / dielectric[sites[nbList[2 * d]]] + 1.0 / dielectric[sites[i]]);
        }
    }
}

//All the simulation initialization happens here.
int init_simulation(Simulation* sim, const char* ini_path){
    INIFile* ini = inip_create();
    inip_load(ini, ini_path);

    unsigned long seed = inip_get_ul(ini, "simulation.seed", time(NULL));
    initMyRand(seed);

    char* solvent_ensemble = inip_get_string(ini, "lattice.ensemble", "grand canonical");
    if(strcmp(solvent_ensemble, "grand canonical") == 0){
        sim->sol_step = solvent_step;
    }
    else if(strcmp(solvent_ensemble, "canonical") == 0){
        sim->sol_step = solvent_canonical_step;
    }
    else{
        printf("Unknown solvent ensemble, '%s'.\nAborting.", solvent_ensemble);
        free(solvent_ensemble);
        return 0;
    }
    free(solvent_ensemble);

    //Lattice initialization
    Lattice* lattice = malloc(sizeof(Lattice));
    int lattice_size[] = {32, 32, 32};
    inip_get_ints(ini, "lattice.size", 3, lattice_size);
    if(!construct_lattice(lattice, lattice_size)){
        free(lattice);
        return 0;
    }
    char* init_type = inip_get_string(ini, "lattice.init", "uniform");
    if(strcmp(init_type, "uniform") == 0){
        double fraction = inip_get_double(ini, "lattice.fraction", 0.5);
        set_sites(lattice, init_lattice_uniform_ratio_one, &fraction);
    }
    else if(strcmp(init_type, "random") == 0){
        double fraction = inip_get_double(ini, "lattice.fraction", 0.5);
        set_sites(lattice, init_lattice_random_ratio_one, &fraction);
    }

    //Need to implement the aborting
    else{
        printf("Lattice initialization type '%s', is not supported.\nAborting.", init_type);
        free(init_type);
        destroy_lattice(lattice);
        free(lattice);
        return 0;
    }

    free(init_type);

    //Plates initialization
    Plates* plates = NULL;
    int size_xy = lattice_size[0] * lattice_size[1];
    int plate_charge = 0;
    if(inip_has_group(ini, "plates")){
        plates = malloc(sizeof(*plates));

        plates->alpha[0] = plates->alpha[1] = 0.8;
        inip_get_doubles(ini, "plates.alpha", 2, plates->alpha);

        //Initialize charge on plates
        plates->charges[0] = malloc(size_xy * sizeof(double));
        plates->charges[1] = malloc(size_xy * sizeof(double));
        plates->total_charge[0] = 10;
        plates->total_charge[1] = 10;
        inip_get_ints(ini, "plates.charge", 2, plates->total_charge);
        plate_charge = plates->total_charge[0] + plates->total_charge[1];
        double charge0 = (double)plates->total_charge[0] / size_xy;
        double charge1 = (double)plates->total_charge[1] / size_xy;
        for(int i = 0; i < size_xy; ++i){
            plates->charges[0][i] = charge0;
            plates->charges[1][i] = charge1;
        }

        draw_plane(lattice, 2, 0, PLATE_A); //Draw z = 0 plane
        draw_plane(lattice, 2, lattice_size[2] - 1, PLATE_B); //Draw z = Lz - 1 plane
    }

    //Ions initialization
    int n_ions[] = {0, 0};
    inip_get_ints(ini, "ions.amount", 2, n_ions);
    int nIons = n_ions[0] + n_ions[1];

    Ions* ions = NULL;
    if(n_ions[0] >= 0 || n_ions[1] >= 0){
        ions = malloc(sizeof(Ions));

        ions->valency[0] = 1;
        ions->valency[1] = 1;
        inip_get_ints(ini, "ions.valency", 2, ions->valency);

        int ion_charge = ions->valency[1] * n_ions[1] - ions->valency[0] * n_ions[0];
        if(nIons > lattice->nSites || (ion_charge + plate_charge) != 0 || !construct_ions(ions, nIons)){
            printf("Problem initializing ions.\n");
            destroy_lattice(lattice);
            free(lattice);
            free(ions);
            free(plates);
            return 0;
        }

        ions->alpha[0] = 0.5;
        ions->alpha[1] = 0.5;
        inip_get_doubles(ini, "ions.alpha", 2, ions->alpha);

        //Set ion charges
        int ion_id = 0;
        for(; ion_id < n_ions[0]; ++ion_id) ions->charge[ion_id] = -ions->valency[0]; //ION_MINUS
        for(; ion_id < nIons; ++ion_id) ions->charge[ion_id] = ions->valency[1]; //ION_PLUS

        //Set ion positions and charge
        for(int ion_type = ION_MINUS; ion_type <= ION_PLUS; ++ion_type){
            int counter = 0;
            while(counter < n_ions[ion_type - ION_MINUS]){
                int idx = mtrandi(lattice->nSites);
                if(lattice->sites[idx] > SOLVENT_B) continue;
                int indices[3];
                indexToIndices(idx, lattice_size, indices);
                int ion_idx = counter + (ion_type - ION_MINUS) * n_ions[0];
                memcpy(ions->position + 3 * ion_idx, indices, 3 * sizeof(*ions->position));
                lattice->sites[idx] = ion_type;

                ++counter;
            }
        }
    }

    //Solutes initialization
    int n_solutes[] = {0, 0};
    inip_get_ints(ini, "solutes.amount", 2, n_solutes);
    int nSolutes = n_solutes[0] + n_solutes[1];

    Solutes* solutes = NULL;
    if(n_solutes[0] >= 0 || n_solutes[1] >= 0){
        solutes = malloc(sizeof(Solutes));
        construct_solutes(solutes, nSolutes);

        solutes->alpha[0] = 0.5;
        solutes->alpha[1] = 0.5;
        inip_get_doubles(ini, "solutes.alpha", 2, solutes->alpha);

        //Set solute positions
        for(int solute_type = SOLUTE_A; solute_type <= SOLUTE_B; ++solute_type){
            int counter = 0;
            while(counter < n_solutes[solute_type - SOLUTE_A]){
                int idx = mtrandi(lattice->nSites);
                if(lattice->sites[idx] > SOLVENT_B) continue;
                int indices[3];
                indexToIndices(idx, lattice_size, indices);
                int solute_idx = counter + (solute_type - SOLUTE_A) * n_solutes[0];
                memcpy(solutes->position + 3 * solute_idx, indices, 3 * sizeof(*solutes->position));
                lattice->sites[idx] = solute_type;
                ++counter;
            }
        }
    }

    sim->lattice = lattice;
    sim->ions    = ions;
    sim->solutes = solutes;
    sim->plates  = plates;

    //Get simulation parameters
    if(inip_has_attribute(ini, "simulation.tau")){
        double tau = inip_get_double(ini, "simulation.tau", 0.0);
        //b_c from Talapov and Blöte (1996)
        sim->params.beta = (4.0 * 0.2216544) / (tau + 1.0);
    }
    else{
        sim->params.beta = inip_get_double(ini, "simulation.beta", 10.0);
    }

    sim->params.mu              = inip_get_double(ini, "simulation.dmu", 0.0) - 3.0;
    sim->params.gamma           = inip_get_double(ini, "simulation.gamma", 0.01);

    sim->params.max_plate       = inip_get_double(ini, "simulation.max_plate", 1.00);
    sim->params.max_field       = inip_get_double(ini, "simulation.max_field", 1.00);
    sim->params.max_Gfield      = inip_get_double(ini, "simulation.max_gfield", 0.015);

    sim->params.worms_per_cycle = inip_get_int(ini, "simulation.worms", 0);

    sim->params.nEqSteps        = inip_get_int(ini, "simulation.equilibrium", 10000);
    sim->params.nOutSteps       = inip_get_int(ini, "simulation.output", 0);
    sim->params.nProdSteps      = inip_get_int(ini, "simulation.production", 10000);

    sim->params.out_dir         = inip_get_string(ini, "simulation.output_dir", "Data/");

    init_fields(sim);

    int nSites = lattice->nSites;
    sim->num_bsites  = get_num_sites(sim->lattice, SOLVENT_B);

    //Init TMMC. We put this here because it can modify the lattice
    TMMC* tmmc = NULL;
    //Initialize these in case MPI is not called.
    sim->tid = 0;
    sim->pid = getpid();
    if(inip_has_group(ini, "TMMC")){
        //initialization cannot fail anymore
        MPI_Init(NULL, NULL);
        MPI_Comm_rank(MPI_COMM_WORLD, &sim->tid);
        MPI_Bcast(&sim->pid, 1, MPI_INT, 0, MPI_COMM_WORLD);

        MPI_Comm_size(MPI_COMM_WORLD, &sim->n_procs);

        tmmc = malloc(sizeof(TMMC));

        tmmc->update_period = inip_get_int(ini, "TMMC.update", 1000);

        int tmmc_n_low  = inip_get_int(ini, "TMMC.lower", 0);
        int tmmc_n_high = inip_get_int(ini, "TMMC.upper", nSites - nIons - nSolutes - 1);

        if(tmmc_n_high > nSites - nIons - nSolutes - 1 || tmmc_n_high < 0) tmmc_n_high = nSites - nIons - nSolutes - 1;

        int tmmc_tot = tmmc_n_high - tmmc_n_low + 1;

        int tmmc_proc_range  = tmmc_tot / sim->n_procs;
        int tmmc_proc_rem    = tmmc_tot % sim->n_procs;

        tmmc->n_low  = tmmc_n_low + tmmc_proc_range * sim->tid + ((tmmc_proc_rem > sim->tid)? sim->tid: tmmc_proc_rem);
        tmmc->n_high = tmmc->n_low + tmmc_proc_range + (sim->tid < tmmc_proc_rem);
        tmmc->n_tot  = tmmc->n_high - tmmc->n_low + 1;

        tmmc->collection_matrix = calloc(3 * tmmc->n_tot, sizeof(*tmmc->collection_matrix));
        tmmc->state_weights     = calloc(tmmc->n_tot, sizeof(*tmmc->collection_matrix));

        //We need to initialize the lattice such that enough B sites are
        //present to be between TMMC.lower and TMMC.upper.
        //To do this, we either add or delete B sites randomly.
        while(sim->num_bsites < tmmc->n_low){
            int sid = mtrandi(nSites);
            if(sim->lattice->sites[sid] == SOLVENT_A){
                sim->lattice->sites[sid] = SOLVENT_B;
                ++sim->num_bsites;
            }
        }

        while(sim->num_bsites > tmmc->n_high){
            int sid = mtrandi(nSites);
            if(sim->lattice->sites[sid] == SOLVENT_B){
                sim->lattice->sites[sid] = SOLVENT_A;
                --sim->num_bsites;
            }
        }
    }

    sim->tmmc = tmmc;

    //Set dielectric constants
    for(int i = 0; i < N_SITE_TYPES; ++i) sim->params.dielectric[i] = 1.0;

    inip_get_doubles(ini, "lattice.dielectric", 2, sim->params.dielectric);
    inip_get_doubles(ini, "ions.dielectric", 2, sim->params.dielectric + ION_MINUS);
    inip_get_doubles(ini, "plates.dielectric", 2, sim->params.dielectric + PLATE_A);
    inip_get_doubles(ini, "solutes.dielectric", 2, sim->params.dielectric + SOLUTE_A);
    init_dielectric(sim);

    sim->total_field[0] = 0.0; sim->total_field[1] = 0.0; sim->total_field[2] = 0.0;
    sim->total_idielectric[0] = 0.0; sim->total_idielectric[1] = 0.0; sim->total_idielectric[2] = 0.0;

    for(int i = 0; i < nSites; ++i){
        for(int d = 0; d < 3; ++d){
            sim->total_field[d] += lattice->fields[d][i] * lattice->idielectric[d][i];
            sim->total_idielectric[d] += lattice->idielectric[d][i];
        }
    }

    sim->dE = 0.0;
    sim->base_energy = calc_energy(sim);

    Measurement** measurements = NULL;
    sim->n_measurements = 0;
    if(inip_has_group(ini, "measurements")){
        measurements = malloc(N_MEASUREMENTS * sizeof(Measurement*));
        if(inip_has_attribute(ini, "measurements.rho_z")){
            measurements[sim->n_measurements] = &ms_rho_z;
            ++sim->n_measurements;
            int data[2] = {0};
            inip_get_ints(ini, "measurements.rho_z", 2, data);
            ms_rho_z.update_period = data[0];
            ms_rho_z.output_period = data[1];
        }
        if(inip_has_attribute(ini, "measurements.energy")){
            measurements[sim->n_measurements] = &ms_energy;
            ++sim->n_measurements;
            int data[2] = {0};
            inip_get_ints(ini, "measurements.energy", 2, data);
            ms_energy.update_period = data[0];
            ms_energy.output_period = data[1];
        }
        if(inip_has_attribute(ini, "measurements.field")){
            measurements[sim->n_measurements] = &ms_field;
            ++sim->n_measurements;
            int data[2] = {0};
            inip_get_ints(ini, "measurements.field", 2, data);
            ms_field.update_period = data[0];
            ms_field.output_period = data[1];
        }
        if(inip_has_attribute(ini, "measurements.tmmc")){
            measurements[sim->n_measurements] = &ms_tmmc;
            ++sim->n_measurements;
            int data[2] = {0};
            inip_get_ints(ini, "measurements.tmmc", 2, data);
            ms_tmmc.update_period = data[0];
            ms_tmmc.output_period = data[1];
        }
    }
    sim->measurements = measurements;

    bool copy_ini = (strcmp("true", inip_get_string(ini, "simulation.copy_ini", "false")) == 0);
    if(copy_ini && sim->tid == 0){
        char out_path[128];
        sprintf(out_path, "%s/sim.pid%d.ini", sim->params.out_dir, sim->pid);
        FILE* fin  = fopen(ini_path, "rb");
        FILE* fout = fopen(out_path, "wb");

        size_t n, m;
        unsigned char buffer[8192];
        do{
            n = fread(buffer, 1, sizeof(buffer), fin);
            if(n) m = fwrite(buffer, 1, n, fout);
            else m = 0;
        }while((n > 0) && (n == m));
        if(m) perror("Error while copying ini file.\n");

        fclose(fout);
        fclose(fin);
    }

    inip_destruct(ini);

    return 1;
}

void release_simulation(Simulation* sim){
    destroy_lattice(sim->lattice);
    if(sim->ions) destroy_ions(sim->ions);
    if(sim->solutes) destroy_solutes(sim->solutes);
    free(sim->lattice);
    free(sim->ions);
    free(sim->solutes);
    free(sim->params.out_dir);
    free(sim->measurements);
    if(sim->plates){
        free(sim->plates->charges[0]);
        free(sim->plates->charges[1]);
        free(sim->plates);
    }
    if(sim->tmmc){
        free(sim->tmmc->collection_matrix);
        free(sim->tmmc->state_weights);
        free(sim->tmmc);
        MPI_Finalize();
    }
}

//In the future we are able to take a function `void (*func)(const Buffer*)`
//to stream data to the outside world
static inline void save_config(const Simulation* sim, const char* filename){
    FILE* file = fopen(filename, "wb");
    Buffer* buffer = buffer_create();
    serialize_lattice(sim->lattice, buffer, 1);
    buffer_reserve(buffer, 3 * sizeof(*sim->total_field));
    buffer_write(buffer, sim->total_field, 3 * sizeof(*sim->total_field));
    fwrite(buffer->data, buffer->size, 1, file);
    fclose(file);
    buffer_destroy(buffer);
}

void run_simulation(Simulation* sim){
    int worms_per_cycle = sim->params.worms_per_cycle;

    int nSites   = sim->lattice->nSites;
    int nIons    = (sim->ions)? sim->ions->nIons: 0;
    int nSolutes = (sim->solutes)? sim->solutes->nSolutes: 0;
    int nEq      = sim->params.nEqSteps;
    int nProd    = sim->params.nProdSteps;
    int nOut     = sim->params.nOutSteps;
    int size_xy  = sim->lattice->size[0] * sim->lattice->size[1];

    for(int ms = 0; ms < sim->n_measurements; ++ms) sim->measurements[ms]->init(sim);

    bool has_plates = (sim->plates != NULL);
    int plaq_base   = (has_plates)? 3 * size_xy: 0;
    int plaq_tot    = 3 * nSites - ((has_plates)? 8 * size_xy: 0);

    //Equilibration run
    int accepted_field  = 0;
    int accepted_global = 0;
    int accepted_plate  = 0;
    for(int i = 0; i < nEq; ++i){
        for(int j = 0; j < nSites + worms_per_cycle; ++j){
            sim->sol_step(sim, mtrandi(nSites));
            int site_id = mtrandi(plaq_tot) + plaq_base;
            accepted_field += field_step(sim, site_id);
        }
        for(int j = 0; j < nIons; ++j){
            ion_step(sim, mtrandi(nIons));
            if(has_plates){
                accepted_plate += plate_step(sim, mtrandi(2 * size_xy));
            }
        }
        for(int j = 0; j < nSolutes; ++j) solute_step(sim, mtrandi(nSolutes));

        for(int j = 0; j < worms_per_cycle; ++j){
            int site = (has_plates)? mtrandi(nSites - 2 * size_xy) + size_xy: mtrandi(nSites);
            worm_step(sim, site);
        }

        accepted_global += global_field_step(sim);

        //Equilibrate maximum field perturbation
        //printf("%f\t%f\n", calc_energy(sim), sim->base_energy + sim->dE);
        if((i + 1) % 100 == 0){
            double acceptance_field  = accepted_field / (100.0 * nSites);
            if(acceptance_field < 0.5) sim->params.max_field *= 0.97;
            else sim->params.max_field *= 1.03;
            accepted_field  = 0;

            double acceptance_global = accepted_global / 100.0;
            if(acceptance_global < 0.3) sim->params.max_Gfield *= 0.97;
            else sim->params.max_Gfield *= 1.03;
            accepted_global = 0;

            if(sim->plates){
                double acceptance_plate = accepted_plate / (100.0 * nIons);
                if(acceptance_plate < 0.6) sim->params.max_plate *= 0.97;
                else sim->params.max_plate *= 1.03;
                accepted_plate = 0;
            }
        }
    }

    //reset energy book-keeping
    sim->base_energy = calc_energy(sim);
    sim->dE = 0.0;

    //Production run
    for(int i = 0; i < nProd; ++i){
        for(int j = 0; j < nSites + worms_per_cycle; ++j){
            sim->sol_step(sim, mtrandi(nSites));
            int site_id = mtrandi(plaq_tot) + plaq_base;
            field_step(sim, site_id);
        }
        for(int j = 0; j < nIons; ++j){
            ion_step(sim, mtrandi(nIons));
            if(has_plates){
                plate_step(sim, mtrandi(2 * size_xy));
            }
        }
        for(int j = 0; j < nSolutes; ++j) solute_step(sim, mtrandi(nSolutes));

        for(int j = 0; j < worms_per_cycle; ++j){
            int site = (has_plates)? mtrandi(nSites - 2 * size_xy) + size_xy: mtrandi(nSites);
            worm_step(sim, site);
        }

        global_field_step(sim);

        if(sim->tmmc && ((i + 1) % sim->tmmc->update_period == 0)){
            update_weights(sim);
        }

        if(nOut && (i + 1) % nOut == 0){
            char filename[64];
            sprintf(filename, "%s/lattice.pid%d.tid%d.step%09d.dat", sim->params.out_dir, sim->pid, sim->tid, i);
            FILE* file = fopen(filename, "wb");
            Buffer* buffer = buffer_create();
            serialize_lattice(sim->lattice, buffer, 0);
            fwrite(buffer->data, buffer->size, 1, file);
            fclose(file);
            buffer_destroy(buffer);
            //NOTE: Use the following function if you also want to output the
            //electric field at each lattice link.
            //save_config(sim, filename);
        }

        for(int ms = 0; ms < sim->n_measurements; ++ms) sim->measurements[ms]->measure(sim, i);
    }

    for(int ms = 0; ms < sim->n_measurements; ++ms) sim->measurements[ms]->release(sim);
}

