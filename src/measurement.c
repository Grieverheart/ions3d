#include "measurement.h"
#include "lattice.h"
#include "ions.h"
#include "solutes.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <mpi.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

typedef struct{
    double* density[N_SITE_TYPES];
}ms_rho_z_data;

static inline void ms_rho_z_init(const Simulation* sim){
    if(ms_rho_z.update_period == 0) return;

    ms_rho_z_data* data = malloc(sizeof(ms_rho_z_data));
    ms_rho_z.data = data;
    for(int i = 0; i < N_SITE_TYPES; ++i){
        data->density[i] = calloc(sim->lattice->size[2], sizeof(double));
    }
}

static inline void ms_rho_z_release(const Simulation* sim){
    if(ms_rho_z.update_period == 0) return;
    for(int i = 0; i < N_SITE_TYPES; ++i){
        free(((ms_rho_z_data*)ms_rho_z.data)->density[i]);
    }
    free(ms_rho_z.data);
}

static inline void ms_rho_z_measure(const Simulation* sim, int step){
    if(!ms_rho_z.update_period || ((step + 1) % ms_rho_z.update_period != 0)) return;

    ms_rho_z_data* data = ms_rho_z.data;


    int size_xy = sim->lattice->size[0] * sim->lattice->size[1];
    int cm_z = 0;
    if(!sim->plates)
    {
        double factor = 2.0 * M_PI / sim->lattice->size[2];

        double xi = 0.0;
        double zeta = 0.0;
        int n_part = 0;
        for(int z = 0, idx = 0; z < sim->lattice->size[2]; ++z){
            double cosz = cos(factor * z);
            double sinz = sin(factor * z);
            int n_part_xy = 0;
            for(int xy = 0; xy < size_xy; ++xy, ++idx){
                if(sim->lattice->sites[idx] == 0) continue;
                ++n_part_xy;
            }
            xi   -= n_part_xy * cosz;
            zeta -= n_part_xy * sinz;
            n_part += n_part_xy;
        }

        cm_z = (atan2(zeta / n_part, xi / n_part) + M_PI) / factor;
    }

    for(int idx = 0; idx < sim->lattice->nSites; ++idx){
        int z = (idx / size_xy - cm_z + sim->lattice->size[2]) % sim->lattice->size[2];
        ++data->density[sim->lattice->sites[idx]][z];
    }

    if(ms_rho_z.output_period && ((step + 1) % ms_rho_z.output_period == 0)){
        char filename[64];
        sprintf(filename, "%s/rho_z.pid%d.tid%d.step%09d.dat", sim->params.out_dir, getpid(), sim->tid, step);
        FILE* data_file = fopen(filename, "w");
        double normalization =  ms_energy.update_period / (size_xy * ms_energy.output_period);
        for(int z = 0; z < sim->lattice->size[2]; ++z){
            for(int site_type = 0; site_type < N_SITE_TYPES; ++site_type){
                fprintf(data_file, "%e\t", data->density[site_type][z] * normalization);
                data->density[site_type][z] = 0;
            }
            fprintf(data_file, "\n");
        }
        fclose(data_file);
    }
}

Measurement ms_rho_z = {
    .update_period  = 0,
    .output_period  = 0,
    .init    = ms_rho_z_init,
    .release = ms_rho_z_release,
    .measure = ms_rho_z_measure,
    .data    = NULL
};

typedef struct{
    FILE*  fp;
    double energy;
}ms_energy_data;

static inline void ms_energy_init(const Simulation* sim){
    if(ms_energy.output_period == 0) return;

    ms_energy_data* data = malloc(sizeof(ms_energy_data));
    ms_energy.data = data;

    data->energy = 0.0;

    char filename[64];
    sprintf(filename, "%s/energy.pid%d.tid%d.dat", sim->params.out_dir, getpid(), sim->tid);
    data->fp = fopen(filename, "w");
}

static inline void ms_energy_release(const Simulation* sim){
    if(ms_energy.output_period == 0) return;
    fclose(((ms_energy_data*)ms_energy.data)->fp);
    free(ms_energy.data);
}

static inline void ms_energy_measure(const Simulation* sim, int step){
    if(ms_energy.output_period == 0) return;

    ms_energy_data* data = ms_energy.data;

    if(ms_energy.update_period && ((step + 1) % ms_energy.update_period == 0)){
        data->energy += (sim->base_energy + sim->dE) / sim->lattice->nSites;
    }

    if(ms_energy.output_period && ((step + 1) % ms_energy.output_period == 0)){
        if(!ms_energy.update_period){
            data->energy = (sim->base_energy + sim->dE) / sim->lattice->nSites;
        }
        else{
            int n_measurements = ms_energy.output_period / ms_energy.update_period;
            data->energy /= n_measurements;
        }
        fprintf(data->fp, "%d\t%e\n", step, data->energy);
        fflush(data->fp);
        data->energy = 0.0;
    }
}

Measurement ms_energy = {
    .update_period = 0,
    .output_period = 0,
    .init          = ms_energy_init,
    .release       = ms_energy_release,
    .measure       = ms_energy_measure,
    .data          = NULL
};

typedef struct{
    FILE*  fp;
    double total_field[3];
}ms_field_data;

static inline void ms_field_init(const Simulation* sim){
    if(ms_field.output_period == 0) return;

    ms_field_data* data = malloc(sizeof(ms_field_data));
    ms_field.data = data;

    for(int d = 0; d < 3; ++d) data->total_field[d] = 0.0;

    char filename[64];
    sprintf(filename, "%s/field.pid%d.tid%d.dat", sim->params.out_dir, getpid(), sim->tid);
    data->fp = fopen(filename, "w");
}

static inline void ms_field_release(const Simulation* sim){
    if(ms_field.output_period == 0) return;
    fclose(((ms_field_data*)ms_field.data)->fp);
    free(ms_field.data);
}

static inline void ms_field_measure(const Simulation* sim, int step){
    if(ms_field.output_period == 0) return;

    ms_field_data* data = ms_field.data;

    if(ms_field.update_period && ((step + 1) % ms_field.update_period == 0)){
        for(int d = 0; d < 3; ++d) data->total_field[d] += sim->total_field[d] + sim->global_field[d] * sim->total_idielectric[d];
    }

    if(ms_field.output_period && ((step + 1) % ms_field.output_period == 0)){
        if(!ms_field.update_period){
            for(int d = 0; d < 3; ++d) data->total_field[d] = sim->total_field[d] + sim->global_field[d] * sim->total_idielectric[d];
        }
        else{
            int n_measurements = ms_field.output_period / ms_field.update_period;
            for(int d = 0; d < 3; ++d) data->total_field[d] /= n_measurements;
        }
        fprintf(data->fp, "%d\t%e\t%e\t%e\n", step, data->total_field[0], data->total_field[1], data->total_field[2]);
        fflush(data->fp);
        for(int d = 0; d < 3; ++d) data->total_field[d] = 0.0;
    }
}

Measurement ms_field = {
    .update_period = 0,
    .output_period = 0,
    .init          = ms_field_init,
    .release       = ms_field_release,
    .measure       = ms_field_measure,
    .data          = NULL
};


//TODO: Implement the measurement of TMMC data
//We don't really care about the update_period as that is basically handled by the simulation.
//We should also make sure the output_period is smaller than period at which the tmmc weights
//are being updated.
typedef struct{
    char filename[64];
    //NOTE: Perhaps will a save previous tmmc measurement for error calculation
    int tmmc_write_offset;
}ms_tmmc_data;

static inline void ms_tmmc_init(const Simulation* sim){
    if(!sim->tmmc) ms_tmmc.output_period = 0;
    if(ms_tmmc.output_period == 0) return;

    ms_tmmc_data* data = malloc(sizeof(ms_tmmc_data));
    ms_tmmc.data = data;
    sprintf(data->filename, "%s/tmmc.pid%d.dat", sim->params.out_dir, sim->pid);

    int* tmmc_pp_n_tot = malloc(sim->n_procs * sizeof(int));
    MPI_Allgather(&sim->tmmc->n_tot, 1, MPI_INT,
                  tmmc_pp_n_tot, 1, MPI_INT,
                  MPI_COMM_WORLD);

    data->tmmc_write_offset = 0;
    tmmc_pp_n_tot[0] += 1;
    for(int i = 0; i < sim->tid; ++i){
        data->tmmc_write_offset += tmmc_pp_n_tot[i] - 1;
    }

    free(tmmc_pp_n_tot);
}

static inline void ms_tmmc_release(const Simulation* sim){
    if(ms_tmmc.output_period == 0) return;
    free(ms_tmmc.data);
}

static inline void ms_tmmc_measure(const Simulation* sim, int step){
    if(!ms_tmmc.output_period || ((step + 1) % ms_tmmc.output_period != 0)) return;

    ms_tmmc_data* data = ms_tmmc.data;

    double* edge_weights = malloc(sim->n_procs * sizeof(double));

    MPI_Allgather(sim->tmmc->state_weights + sim->tmmc->n_tot - 1, 1, MPI_DOUBLE,
                  edge_weights, 1, MPI_DOUBLE,
                  MPI_COMM_WORLD);

    double window_offset = 0.0;
    for(int j = 0; j < sim->tid; ++j){
        window_offset += edge_weights[j];
    }

    free(edge_weights);

    //Copy, because modifying the original could cause some precision loss
    double* weights = malloc(sim->tmmc->n_tot * sizeof(*sim->tmmc->state_weights));
    for(int n = 0; n < sim->tmmc->n_tot; ++n){
        weights[n] = sim->tmmc->state_weights[n] + window_offset;
    }

    MPI_File data_file;
    MPI_File_open(MPI_COMM_WORLD, data->filename, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &data_file);
    MPI_File_seek(data_file, data->tmmc_write_offset * sizeof(double), MPI_SEEK_SET);
    if(sim->tid == 0) MPI_File_write(data_file, weights, sim->tmmc->n_tot, MPI_DOUBLE, MPI_STATUS_IGNORE);
    else MPI_File_write(data_file, weights + 1, sim->tmmc->n_tot - 1, MPI_DOUBLE, MPI_STATUS_IGNORE);
    MPI_File_close(&data_file);
    free(weights);
}

Measurement ms_tmmc = {
    .update_period  = 0,
    .output_period  = 0,
    .init    = ms_tmmc_init,
    .release = ms_tmmc_release,
    .measure = ms_tmmc_measure,
    .data    = NULL
};


