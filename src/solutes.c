#include "solutes.h"
#include <stdlib.h>
#include <stdio.h>

int construct_solutes(Solutes* solutes, int nSolutes){
    solutes->nSolutes = nSolutes;
    solutes->position = malloc(3 * nSolutes * sizeof(*solutes->position));

    if(!solutes->position){
        fprintf(stderr, "Error constructing Solutes: Memory allocation error.\n");
        return 0;
    }

    return 1;
}

void destroy_solutes(Solutes* solutes){
    free(solutes->position);
}
