#include "serialization_buffer.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#define BUFFER_INIT_SIZE 32

void buffer_write(Buffer* buffer, const void* ptr, size_t size){
    memcpy(buffer->data + buffer->position, ptr, size);
    buffer->position += size;
}

Buffer* buffer_create(void){
    Buffer* buffer = malloc(sizeof(Buffer));
    if(!buffer) fprintf(stderr, "ERROR: Could not allocate serialization buffer!\n");

    buffer->size     = 0;
    buffer->position = 0;
    buffer->reserve  = BUFFER_INIT_SIZE;
    buffer->data     = malloc(buffer->reserve);

    return buffer;
}

void buffer_destroy(Buffer* buffer){
    free(buffer->data);
    free(buffer);
}

void buffer_reserve(Buffer* buffer, size_t size){
    buffer->size += size;
    if(buffer->position + size > buffer->reserve){
        buffer->reserve = 2 * buffer->size;
        buffer->data = realloc(buffer->data, buffer->reserve);
        if(!buffer->data) fprintf(stderr, "Could not reallocate serialization buffer memory\n");
    }
}
