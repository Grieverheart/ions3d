#include <stdio.h>
#include "simulation.h"

int main(int argc, char* argv[]){

    if(argc < 2) printf("Please provide an .ini file.\n");

    Simulation simulation;

    if(init_simulation(&simulation, argv[1])){

        run_simulation(&simulation);

        release_simulation(&simulation);
    }

    return 0;
}
