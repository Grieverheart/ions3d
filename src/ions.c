#include "ions.h"
#include <stdlib.h>
#include <stdio.h>

int construct_ions(Ions* ions, int nIons){
    ions->nIons    = nIons;
    ions->position = malloc(3 * nIons * sizeof(*ions->position));
    ions->charge   = malloc(nIons * sizeof(*ions->charge));

    if(!ions->position || !ions->charge){
        fprintf(stderr, "Error constructing Ions: Memory allocation error.\n");
        return 0;
    }

    return 1;
}

void destroy_ions(Ions* ions){
    free(ions->position);
    free(ions->charge);
}
