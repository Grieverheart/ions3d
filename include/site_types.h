#ifndef __SITE_TYPES_H
#define __SITE_TYPES_H

enum eSiteTypes{
    SOLVENT_A    = 0, //0000
    SOLVENT_B    = 1, //0001
    ION_MINUS    = 2, //0010
    ION_PLUS     = 3, //0011
    PLATE_A      = 4, //0100
    PLATE_B      = 5, //0101
    SOLUTE_A     = 6, //0110
    SOLUTE_B     = 7, //0111
    N_SITE_TYPES = 8  //1000
};

#endif
