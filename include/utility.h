#ifndef UTILITY_H
#define UTILITY_H

#include "exp_table.h"

static inline double min(double a, double b){
    return (a < b)? a: b;
}

static inline double sqr_d(double a){
    return a * a;
}

static inline int apply_BC(int index, int size){
    return (index < 0)? index + size: (index >= size)? index - size: index;
}

//Fast algorithm for calculating metropolis criterion
//Takes a random number rand_num in [0, 1) and the argument x, of exp(x)
static inline int metropolis(double rand_num, double x){
    int exp_i = -(int)x;
    if(exp_i > 699) return 0;
    double prefactor = (exp_i > 0)? exp_table[exp_i - 1]: 1.0;
    double k = x + exp_i;
    double f = k;
    double est = 1.0 + k;
    int i = 1;
    while(1){
        if(rand_num < prefactor * est) return 1;
        f *= k / (i + 1);
        est += f;
        if(rand_num > prefactor * est) return 0;
        f *= k / (i + 2);
        est += f;
        i += 2;
    }
    return 0;
}

#endif
