#ifndef FMATH_H
#define FMATH_H

#include <math.h>
#include <stdint.h>
#include <string.h>
#if defined(_WIN32) && !defined(__GNUC__)
	#include <intrin.h>
	#ifndef MIE_ALIGN
		#define MIE_ALIGN(x) __declspec(align(x))
	#endif
#else
	#ifndef __GNUC_PREREQ
	#define __GNUC_PREREQ(major, minor) ((((__GNUC__) << 16) + (__GNUC_MINOR__)) >= (((major) << 16) + (minor)))
	#endif
	#if __GNUC_PREREQ(4, 4) || !defined(__GNUC__)
		/* GCC >= 4.4 and non-GCC compilers */
		#include <x86intrin.h>
	#elif __GNUC_PREREQ(4, 1)
		/* GCC 4.1, 4.2, and 4.3 do not have x86intrin.h, directly include SSE2 header */
		#include <emmintrin.h>
	#endif
	#ifndef MIE_ALIGN
		#define MIE_ALIGN(x) __attribute__((aligned(x)))
	#endif
#endif
#ifndef MIE_PACK
	#define MIE_PACK(x, y, z, w) ((x) * 64 + (y) * 16 + (z) * 4 + (w))
#endif

#define EXPD_TABLE_SIZE 11

enum{
    EXPD_SBIT = EXPD_TABLE_SIZE,
    EXPD_S    = 1UL << EXPD_SBIT,
    EXPD_ADJ  = (1UL << (EXPD_SBIT + 10)) - (1UL << EXPD_SBIT),
    EXPD_MASK = (1U << EXPD_SBIT) - 1
};

typedef union{
    double   d;
    uint64_t i;
}di;

typedef struct{
    // A = 1, B = 1, C = 1/2, D = 1/6
    double C1[2]; // A
    double C2[2]; // D
    double C3[2]; // C/D
    uint64_t tbl[EXPD_S];
    double a;
    double ra;
}ExpdVar;

static inline double fmath_exp(double x){
    static const ExpdVar c = {
        .a  = EXPD_S / 0.69314718055994530941723212145818,
        .ra = 0.69314718055994530941723212145818 / EXPD_S,
#if 0
        .C1 = {1.0, 1.0},
        .C2 = {0.16667794882310216, 0.16667794882310216},
        .C3 = {2.9997969303278795, 2.9997969303278795},
#else
        .C1 = {1.0, 1.0},
        .C2 = {0.16666666685227835064, 0.16666666685227835064},
        .C3 = {3.0000000027955394, 3.0000000027955394},
#endif
        .tbl = {
#include "fmath_expd_tbl11.h"
        }
    };
    if(x <= -708.39641853226408) return 0;
    if(x >=  709.78271289338397) return HUGE_VAL;
#if 0
    const double _b = (double)((uint64_t)(3) << 51);
    __m128d b  = _mm_load_sd(&_b);
    __m128d xx = _mm_load_sd(&x);
    __m128d d  = _mm_add_sd(_mm_mul_sd(xx, _mm_load_sd(&c.a)), b);
    uint64_t di = _mm_cvtsi128_si32(_mm_castpd_si128(d));
    uint64_t iax = c.tbl[di & EXPD_MASK];
    __m128d _t = _mm_sub_sd(_mm_mul_sd(_mm_sub_sd(d, b), _mm_load_sd(&c.ra)), xx);
    uint64_t u = ((di + EXPD_ADJ) >> EXPD_SBIT) << 52;
    double t;
    _mm_store_sd(&t, _t);
    double y = (c.C3[0] - t) * (t * t) * c.C2[0] - t + c.C1[0];
    double did;
    u |= iax;
    memcpy(&did, &u, sizeof(did));
    return y * did;
#else
/*
    remark : -ffast-math option of gcc may generate bad code for fmath::expd
*/
    static const uint64_t b = 3ULL << 51;
    di di;
    di.d = x * c.a + b;
    uint64_t iax = c.tbl[di.i & EXPD_MASK];

    double   t = (di.d - b) * c.ra - x;
    uint64_t u = ((di.i + EXPD_ADJ) >> EXPD_SBIT) << 52;
    double   y = (c.C3[0] - t) * (t * t) * c.C2[0] - t + c.C1[0];

    di.i = u | iax;

    return y * di.d;
#endif
}

static inline void fmath_exp_v(double *px, size_t n)
{
    static const ExpdVar c = {
        .a  = EXPD_S / 0.69314718055994530941723212145818,
        .ra = 0.69314718055994530941723212145818 / EXPD_S,
#if 0
        .C1 = {1.0, 1.0},
        .C2 = {0.16667794882310216, 0.16667794882310216},
        .C3 = {2.9997969303278795, 2.9997969303278795},
#else
        .C1 = {1.0, 1.0},
        .C2 = {0.16666666685227835064, 0.16666666685227835064},
        .C3 = {3.0000000027955394, 3.0000000027955394},
#endif
        .tbl = {
#include "fmath_expd_tbl11.h"
        }
    };
	const double b = (double)(3ULL << 51);
#ifdef __AVX2__
	assert((n % 4) == 0);
	const __m256d mC1      = _mm256_set1_pd(c.C1[0]);
	const __m256d mC2      = _mm256_set1_pd(c.C2[0]);
	const __m256d mC3      = _mm256_set1_pd(c.C3[0]);
	const __m256d ma       = _mm256_set1_pd(c.a);
	const __m256d mra      = _mm256_set1_pd(c.ra);
	const __m256i madj     = _mm256_set1_epi64x(EXPD_ADJ);
	const __m256i maskSbit = _mm256_set1_epi64x(EXPD_MASK);
	const __m256d expMax   = _mm256_set1_pd(709.78272569338397);
	const __m256d expMin   = _mm256_set1_pd(-708.39641853226408);

	for (size_t i = 0; i < n; i += 4) {
		__m256d x = _mm256_load_pd(px);
		x = _mm256_min_pd(x, expMax);
		x = _mm256_max_pd(x, expMin);

		__m256d d = _mm256_mul_pd(x, ma);
		d = _mm256_add_pd(d, _mm256_set1_pd(b));
		__m256i adr = _mm256_and_si256(_mm256_castpd_si256(d), maskSbit);
		__m256i iax = _mm256_i64gather_epi64((const long long*)c.tbl, adr, 8);
		__m256d t = _mm256_sub_pd(_mm256_mul_pd(_mm256_sub_pd(d, _mm256_set1_pd(b)), mra), x);
		__m256i u = _mm256_castpd_si256(d);
		u = _mm256_add_epi64(u, madj);
		u = _mm256_srli_epi64(u, EXPD_SBIT);
		u = _mm256_slli_epi64(u, 52);
		u = _mm256_or_si256(u, iax);
		__m256d y = _mm256_mul_pd(_mm256_sub_pd(mC3, t), _mm256_mul_pd(t, t));
		y = _mm256_mul_pd(y, mC2);
		y = _mm256_add_pd(_mm256_sub_pd(y, t), mC1);
		_mm256_store_pd(px, _mm256_mul_pd(y, _mm256_castsi256_pd(u)));
		px += 4;
	}
#else
	assert((n % 2) == 0);
	const __m128d mC1 = _mm_set1_pd(c.C1[0]);
	const __m128d mC2 = _mm_set1_pd(c.C2[0]);
	const __m128d mC3 = _mm_set1_pd(c.C3[0]);
	const __m128d ma  = _mm_set1_pd(c.a);
	const __m128d mra = _mm_set1_pd(c.ra);
#if defined(__x86_64__) || defined(_WIN64)
	const __m128i madj = _mm_set1_epi64x(EXPD_ADJ);
#else
	const __m128i madj = _mm_set_epi32(0, EXPD_ADJ, 0, EXPD_ADJ);
#endif
	const __m128d expMax = _mm_set1_pd( 709.78272569338397);
	const __m128d expMin = _mm_set1_pd(-708.39641853226408);
	for (size_t i = 0; i < n; i += 2) {
		__m128d x = _mm_load_pd(px);
		x = _mm_min_pd(x, expMax);
		x = _mm_max_pd(x, expMin);

		__m128d d = _mm_mul_pd(x, ma);
		d = _mm_add_pd(d, _mm_set1_pd(b));
		int adr0 = _mm_cvtsi128_si32(_mm_castpd_si128(d)) & EXPD_MASK;
		int adr1 = _mm_cvtsi128_si32(_mm_srli_si128(_mm_castpd_si128(d), 8)) & EXPD_MASK;

		__m128i iaxL = _mm_castpd_si128(_mm_load_sd((const double*)&c.tbl[adr0]));
		__m128i iax  = _mm_castpd_si128(_mm_load_sd((const double*)&c.tbl[adr1]));
		iax          = _mm_unpacklo_epi64(iaxL, iax);

		__m128d t = _mm_sub_pd(_mm_mul_pd(_mm_sub_pd(d, _mm_set1_pd(b)), mra), x);
		__m128i u = _mm_castpd_si128(d);
		u = _mm_add_epi64(u, madj);
		u = _mm_srli_epi64(u, EXPD_SBIT);
		u = _mm_slli_epi64(u, 52);
		u = _mm_or_si128(u, iax);
		__m128d y = _mm_mul_pd(_mm_sub_pd(mC3, t), _mm_mul_pd(t, t));
		y = _mm_mul_pd(y, mC2);
		y = _mm_add_pd(_mm_sub_pd(y, t), mC1);
		_mm_store_pd(px, _mm_mul_pd(y, _mm_castsi128_pd(u)));
		px += 2;
	}
#endif
}

#endif
