#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include "simulation.h"

typedef struct measurement_t{
    int  update_period;
    int  output_period;
    void (*init)(const Simulation*);
    void (*release)(const Simulation*);
    void (*measure)(const Simulation*, int step);
    void* data;
}Measurement;

#define N_MEASUREMENTS 4

//Measurements have the ms_ prefix

extern Measurement ms_rho_z;
extern Measurement ms_energy;
extern Measurement ms_field;
extern Measurement ms_tmmc;

#endif
