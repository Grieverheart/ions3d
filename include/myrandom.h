#ifndef MYRANDOM_H
#define MYRANDOM_H

//Mersenne Twister
#define DSFMT_MEXP 19937
#define HAVE_SSE2
#include "dSFMT.h"
//Mersenne Twister

extern dsfmt_t dsfmt;

static inline unsigned int mtrandi(unsigned int a){
    /////////////////////////////////////////
    // Returns a random int from 0 to a-1  //
    /////////////////////////////////////////
    return (unsigned int)(dsfmt_genrand_close_open(&dsfmt) * a);
}

static inline double mtrandf(double a, unsigned int b){
    ///////////////////////////////////////////////////////////////////////
    // Returns a random double in [-a, a) if b is 1 and [0, a) if b is 0 //
    ///////////////////////////////////////////////////////////////////////
    return b? a * (2 * dsfmt_genrand_close_open(&dsfmt) - 1) : a * dsfmt_genrand_close_open(&dsfmt);
}

void initMyRand(unsigned long seed);

#endif
