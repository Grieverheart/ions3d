#ifndef __LATTICE_H
#define __LATTICE_H

#include <string.h>
#include <stdint.h>
#include "site_types.h"

struct ions_t;
struct buffer_t;

typedef int_fast8_t site_t;

typedef struct lattice_t{
    int     nSites;
    int     size[3];
    site_t* sites;
    int*    nbLists;
    double* fields[3];
    double* idielectric[3];
}Lattice;

typedef site_t (*SiteSetter)(int x, int y, int z, const int* sizes, void* data);
site_t init_lattice_uniform_ratio_one(int x, int y, int z, const int* sizes, void* data);
site_t init_lattice_random_ratio_one(int x, int y, int z, const int* sizes, void* data);

void set_sites(Lattice* lattice, SiteSetter setter, void* data);
void draw_ions(Lattice* lattice, const struct ions_t* ions);
void draw_plane(Lattice* lattice, int plane_id, int plane_position, enum eSiteTypes site_type);

int get_num_sites(const Lattice* lattice, site_t site_type);

int  construct_lattice(Lattice* lattice, int* size);
void destroy_lattice(Lattice* lattice);

void serialize_lattice(const Lattice* lattice, struct buffer_t* buffer, int write_field);
//void deserialize_lattice(const char* data, Lattice* lattice);

/* Inline functions */

//NOTE: Checked performance, and found that converting an index to indices
//is faster than generating 3 random numbers
static inline void indexToIndices(int index, const int* sizes, int* indices){
    int dividers[3];
    dividers[0] = 1;
    for(int i = 0; i < 2; ++i){
        dividers[i + 1] = dividers[i] * sizes[i];
    }
    int remainder = index;
    for(int i = 0; i < 3; ++i){
       indices[2 - i] = remainder / dividers[2 - i];
       remainder = remainder % dividers[2 - i];
    }
}

static inline int indicesToIndex(const int* indices, const int* sizes){
    return sizes[0] * (sizes[1] * indices[2] + indices[1]) + indices[0];
}

static inline void swap_sites(site_t* sites, int a, int b){
    site_t temp = sites[a];
    sites[a]    = sites[b];
    sites[b]    = temp;
}


//NOTE: Should zero the array before calling this function
static inline void nb_sums(const Lattice* lattice, int index, int* sums){
    const int* nbList = &lattice->nbLists[6 * index];

    for(int i = 0; i < 6; ++i){
        //This way where the first two cases are merged is faster
        site_t site_type = lattice->sites[nbList[i]];
        ++sums[site_type];
    }
}

#endif
