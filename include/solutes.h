#ifndef __SOLUTES_H
#define __SOLUTES_H

typedef struct solutes_t{
    int    nSolutes;
    int*   position;
    double alpha[2];
}Solutes;

int  construct_solutes(Solutes* solutes, int nSolutes);
void destroy_solutes(Solutes* solutes);

#endif
