#ifndef __IONS_H
#define __IONS_H

typedef struct ions_t{
    int    nIons;
    int*   position;
    int*   charge;
    int    valency[2];
    double alpha[2];
}Ions;

//TODO: Perhaps we should change valency to include sign.

#define CHARGE(ions, site_type) (((site_type) == ION_MINUS)? -(ions)->valency[(site_type) - ION_MINUS]:\
                                 ((site_type) == ION_PLUS)?   (ions)->valency[(site_type) - ION_MINUS]:\
                                                              0)

int  construct_ions(Ions* ions, int nIons);
void destroy_ions(Ions* ions);

#endif
