#ifndef __SIMULATION_H
#define __SIMULATION_H

#include "site_types.h"
#include <stdint.h>

struct lattice_t;
struct ions_t;
struct solutes_t;
struct plates_t;
struct measurement_t;
struct simulation_t;

typedef int (*solvent_step_func)(struct simulation_t*, int);

typedef struct tmmc_t{
    int     n_low;
    int     n_high;
    int     n_tot;
    int     update_period;
    double* collection_matrix;
    double* state_weights;
}TMMC;

typedef struct simulation_t{
    struct lattice_t*      lattice;
    struct ions_t*         ions;
    struct solutes_t*      solutes;
    struct plates_t*       plates;
    struct tmmc_t*         tmmc;
    struct measurement_t** measurements;

    solvent_step_func sol_step;

    double   global_field[3];
    double   total_field[3];
    double   total_idielectric[3];
    double   base_energy;
    double   dE;
    int      num_bsites;
    int      n_measurements;
    int      pid;
    int      tid;
    int      n_procs;

    struct{
        int nEqSteps;
        int nOutSteps;
        int nProdSteps;
        int worms_per_cycle;
        double gamma;
        double beta;
        double mu;
        double max_plate;
        double max_field;
        double max_Gfield;
        double dielectric[N_SITE_TYPES];
        char*  out_dir;
    }params;
}Simulation;

int  init_simulation(Simulation* sim, const char* ini_path);
void run_simulation(Simulation* simulation);
void release_simulation(Simulation* simulation);

#endif
