#ifndef __SERIALIZATION_BUFFER_H
#define __SERIALIZATION_BUFFER_H

#include <stddef.h>

typedef struct buffer_t{
    void*  data;
    size_t position;
    size_t size;
    size_t reserve;
}Buffer;

Buffer* buffer_create(void);
void buffer_destroy(Buffer* buffer);
void buffer_reserve(Buffer* buffer, size_t size);
void buffer_write(Buffer* buffer, const void* ptr, size_t size);

#endif
