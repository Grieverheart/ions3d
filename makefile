SRC=$(wildcard src/*.c)
OBJ=$(patsubst src/%.c, bin/%.o, $(SRC))
EXE=main

ERR = $(shell which icpc >/dev/null; echo $$?)
ifeq "$(ERR)" "0"
	CC=mpicc -cc=icc
	CFLAGS=-Wall -Wextra -Wno-unused-parameter -finline -O3 -std=c99 -g -I./include/ -ansi-alias -ipo -march=native -DNDEBUG
else
	CC=mpicc
	CFLAGS=-Wall -Wextra -Wno-unused-parameter -O3 -std=c99 -g -I./include/ -march=native -flto -DNDEBUG
endif

LDFLAGS=-lm -L. -lini_parser
dSFMTFLAGS=-DDSFMT_MEXP=19937 -DHAVE_SSE2
RM=rm

bin/%.o: src/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

bin/dSFMT.o: src/dSFMT.c
	$(CC) $(CFLAGS) $(dSFMTFLAGS) -o $@ -c $<

.PHONY: all
all: bin/ $(EXE)
	@echo Done

bin/:
	mkdir bin

$(EXE): $(OBJ) $(COBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $@ $(LDFLAGS)

.PHONY: clean
clean:
	-$(RM) $(OBJ)
	@echo Clean Done!
