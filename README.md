# README #

ions3D uses the Monte Carlo algorithm described in **'to be published'** to simulate ions in a binary solvent mixture.

### Compiling ###

ions3D requires MPI to compile.

You will also need to download and compile the ini parser from [here](https://bitbucket.org/Grieverheart/ini_parser). Follow the instructions there. After successfully compiling the ini parser, you can either install it in the system directory, or in the directory of ions3D.

You can then compile ions3D by using make,
```
#!bash
make all
```

### Usage ###

Run a simulation by providing a initialization file as the first argument, e.g.,

```
#!bash
./main example.ini
```

### Documentation ###
Documentation can be found in the example.ini initialization file, and in the source files.